package testng_jenkins;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;



public class testng_jenkins {
	
  @Test(priority = 1, dataProvider = "dp")
  public void verifyPageTitle(Integer n, String s){
	  
	  System.out.println("@Test, priority 1");
	  
	  File file = new File("C:/Jar_folder/selenium-2.53.1/chromedriver.exe");
	  System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
	  WebDriver chr = new ChromeDriver();
	  chr.get("http://www.google.com");
	  String actualTitle = chr.getTitle();
	  String expectedTitle = "Google";
	  Assert.assertEquals(actualTitle, expectedTitle);
	  chr.quit();
  }
  
  @Test(priority = 3)//(dataProvider = "dp")
  public void test2() {
	  System.out.println("@Test, priority 3");
  }
  

  @Test(priority = 2, dataProvider = "dp1")
  public void test3(Integer n, String s) {
	  System.out.println("@Test, priority 2");
	  
  }
  
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("@BeforeMethod");
	  
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("@AfterMethod");
	  
  }


  @DataProvider
  public Object[][] dp() {
	  System.out.println("@DataProvider");
    return new Object[][] {
      new Object[] { 1, "a" }
    };
  }
  
  @DataProvider
  public Object[][] dp1() {
	  System.out.println("@DataProvider1");
    return new Object[][] {
      new Object[] { 3, "c" }
    };
  }
  
  @BeforeClass
  public void beforeClass() {
	  System.out.println("@BeforeClass");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("@AfterClass");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("@BeforeTest");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("@AfterTest");
  }

  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("@BeforeSuite");
  }

  @AfterSuite
  public void afterSuite() {
	  System.out.println("@AfterSuite");
  }

}
