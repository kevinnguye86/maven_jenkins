<%@page import="java.util.Date"%>
<html>
<body>
	<h2>Hello World!</h2>
	<%
		Date d = new Date();
	%>
	<%=d + " is today's date"%>
	<form action="ThirdServlet" method="POST">
		<table>
			<tr>
				<td>First Name:</td>
				<td><input type="text" name="firstname" /></td>
				<td>Last Name:</td>
				<td><input type="text" name="lastname" /></td>
			</tr>
			<tr>
				<td>Birthdate:</td>
				<td><input type="text" name="birthdate" /></td>
				<td>Employee Number:</td>
				<td><input type="text" name="emp_num" /></td>
			</tr>
			<tr>
				<td>Address:</td>
				<td><input type="text" name="address" /></td>
				<td>City:</td>
				<td><input type="text" name="city" /></td>
				<td>State:</td>
				<td><input type="text" name="state" /></td>
			</tr>
			<tr>
				<td><input type="submit" name="submit" value="Submit" /></td>
			</tr>
		</table>

	</form>
	<p>This page will do some ajax stuff</p>
	<div id="debug"></div>
	<div id="ajax"></div>
	<button id="do-ajax">More info</button>
	<p>Only part of the page will be refreshed</p>

	<script src="script.js"></script>

</body>
</html>
