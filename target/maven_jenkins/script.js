﻿//create an XMLHttpRequest Object
var xhr;
//get a reference to the button
var button = document.getElementById("do-ajax");
//get a reference to the ajax box
var aBox = document.getElementById("ajax");

var  doAjax = function(){
  //check for Microsoft
if(XMLHttpRequest){
  xhr = new XMLHttpRequest();
}else{
  //old Microsoft browsers
  xhr  = new ActiveXObject("Microsoft.XMLHTTP");
}

//2. set the readystatechange callback
/* There are 5 states of request
    0. Request not initialized
    1. Server connection established
    2. Request Received
    3. Processing request
    4. Request finish/response ready
*/
xhr.onreadystatechange = function(){
  if(xhr.readyState != 4) return;
  document.getElementById("debug").innerHTML = xhr.status;
  if(xhr.status == 200){
    var res = JSON.parse(xhr.responseText);
    aBox.innerHTML = res;
  }else{
    console.error("An Error");
    aBox.innerHTML = "An error has occured!";
  }
}

//3. Open the connection
xhr.open("GET", "https://pokeapi.co/api/v2/pokemon/25", true);
  
//4. Send the request
  xhr.send();
};

button.onclick = doAjax;

